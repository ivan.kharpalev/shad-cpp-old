#include <testing.h>
#include <util.h>
#include <strict_iterator.h>
#include <local_max.h>
#include <vector>

namespace tests {

void TestAll() {
    StartTesting();
    std::vector<int> v{{1,3}};
    ASSERT_EQ(1, LocalMax(v.begin(), v.end()) - v.begin());
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
