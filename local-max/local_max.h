#pragma once

#include <stdexcept>

template<class Iterator>
Iterator LocalMax(Iterator first, Iterator last) {
    if (first == last)
        return last;
    auto i1 = first, i2 = std::next(first);
    if (i2 == last || *i1 > *i2) {
        return i1;
    }
    for (auto i3 = std::next(first, 2);
            i3 != last; ++i1, ++i2, ++i3) {
        if (*i1 < *i2 && *i2 > *i3) {
            return i2;
        }
    }
    if (*i2 > *i1) {
        return i2;
    }
    return last;
}
