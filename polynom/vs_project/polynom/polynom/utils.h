#pragma once 

#define __FILE__LINE__ __FILE__ << ":" << __LINE__
#define ASSERT_TRUE(x) \
if (!(x)) { \
    auto xValue = (x); \
    std::cerr << "ASSERT: expression <" << #x << "> that has value  <" << xValue << "> is not true at " << __FILE__LINE__ << std::endl; \
}

// display value only if it is possible and show something like "value can not be displayer" otherwise
//  std::cerr << "ASSERT: expression <"  << #x << "> that has value  <" << xValue << "> is not true at " << __FILE__LINE__ << std::endl; \

#define ASSERT_EQ(x, y) \
if (x != y) { \
    auto xValue = x; \
    auto yValue = y; \
std::stringstream ss; \
ss << "ASSERT: <" << #x << "> != <" << #y << ">" << std::endl << \
"or (same in values) <" << xValue << "> != <" << yValue << ">" << " at " << __FILE__LINE__ << std::endl; \
std::cerr << ss.str() << std::endl; \
throw std::runtime_error(ss.str());\
}

#define ASSERT_EQ_PRECISION(x, y, eps) \
if (abs(x - y) > eps) { \
    auto xValue = x; \
    auto yValue = y; \
	auto diff = xValue - yValue; \
std::stringstream ss; \
ss << "ASSERT: <" << #x << "> != <" << #y << "> diff = <" << diff << ">" << std::endl << \
"or (same in values) <" << xValue << "> != <" << yValue << ">" << " at " << __FILE__LINE__ << std::endl; \
std::cerr << ss.str() << std::endl; \
throw std::runtime_error(ss.str());\
}
