﻿#include "utils.h"

#include <vector>
#include <exception>
#include <functional>
#include <iostream>
#include <sstream>
#include <cmath>
#include <initializer_list>
#include <map>
#include <set>
#include <cstring>

//#include <boost/serialization/strong_typedef.hpp>

//class MaxIterationExceptino : public std::runtime_error Х

const double c_rootArgumentPrecision = 1e-6;


#define _c std::cout <<
#define _e << std::endl;

//TODO use template function argument
double GetRoot(std::function<double(double)> func, double l, double r, double epsilon) {
	double fl = func(l), fr = func(r);
	ASSERT_TRUE(fl * fr <= 0);
	//if (fl * fr > 0) {
	//	throw std::runtime_error("wrong function for __FUNCTION__");
	//}
	while (r - l > c_rootArgumentPrecision) {
		double m = (r + l) / 2; // overflow
		double fm = func(m);
		if (fm * fl <= 0) {
			r = m;
			fr = fm;
		}
		else {
			l = m;
			fl = fm;
		}
	}
	return (l + r) / 2;
}

//inline double abs(double x) {
//	return x > 0 ? x : -x;
//}





struct RootTestSet {
	std::function<double(double)> func;
	std::string functionStringPresentation;
	double leftEnd, rightEnd;
	double root;
	double precision;
};

//std::ostream& operator<<(std::ostream& out, const std::map& set) {
//	return out;
//}

std::ostream& operator<<(std::ostream& out, const RootTestSet& set) {
	return out << "exprected root of <" << set.functionStringPresentation << "> at [" << set.leftEnd << ", " << set.rightEnd << "] is <" << set.root << ">" << "with precision = " << set.precision;
}

void runRootTest(const RootTestSet& set) {
	//    try {
	double foundRoot = GetRoot(set.func, set.leftEnd, set.rightEnd, set.precision);
	if (abs(foundRoot - set.root) > c_rootArgumentPrecision) {
		std::stringstream ss;
		ss << std::endl << set << std::endl << "found root is <" << foundRoot << ">";
		throw std::runtime_error(ss.str());
	}
	else {
		std::cout << set.functionStringPresentation << "  -- OK" << std::endl;
	}
}


void GetRootTest() {
	std::cout << "GetRootTest Started" << std::endl;
	runRootTest({ [](double x) {return x - 1; }, "x-1", -1, 3., 1, 1e-6});
	runRootTest({ [](double x) {return x*1e-5 - 1e-6; }, "x*1e-5 - 1e-6", -1, 3., 1e-1, 1e-6 });
	runRootTest({ [](double x) {return std::sin(x - 1); }, "sin(x-1)", -1, 3., 1, 1e-6 });
	runRootTest({ [](double x) {return x - 1; }, "-x+1", -1, 3., 1, 1e-6 });
	//std::cout << "GetRootTest all tests passed OK" << std::endl;

	//    auto xFunc = [](double x) { return x;};
	//    double root = GetRoot(xFunc, -1, 1);
	//    if (abs(root) < c_rootArgumentPrecision) {
	//        std::cout << "root found" << std::endl;
	//    }
	//    ASSERT_TRUE(0);
	//    ASSERT_EQ(1+1, 2.1)
}




struct Segment {
	double left, right;
};

class Seq {
public:
	class Iterator {

	};
	Iterator begin() {}
	Iterator end() {}

	Seq mergeAdd(Segment&& more) {

	}
};

template<typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec) {
	out << "(";
	if (!vec.empty()) {
		out << vec[0];
		for (int i = 1; i < vec.size(); ++i) {
			out << ", " << vec[i];
		}
	}
	return out << ")";
}
// Question std::clone method  do [](auto auto x = oldVar; return x;

struct A {
	A() {
		std::cout << "A()" << std::endl;
	}

	template<typename T>
	A(std::initializer_list<T> il) {
		std::vector<T> v(il.begin(), il.end());
		std::cout << "A(std::initializer_litst<T> il)  called" << std::endl;
	}
	A(const std::vector<int>&) {
		std::cout << "A(const std::vector<int>&)" << std::endl;
	}
};

bool isIn(double left, double right, double x) {
	return left <= x && x <= right;
}

//BOOST_STRONG_TYPEDEF(std::map<double, int>, PolynomRoots);
using PolynomRoots = std::map<double, int>;

std::ostream& operator<<(std::ostream& out, const PolynomRoots& roots) {
	out << "(";
	for (const auto& root : roots) {
		out << "{" << root.first << " x " << std::to_string(root.second) << "}, ";
	}
	return out << ")";
}

bool operator==(const PolynomRoots& r1, const PolynomRoots& r2) {
	if (r1.size() != r2.size()) {
		return false;
	}
	for (auto r1it = r1.begin(), r2it = r2.begin(); r1it != r1.end(); ++r1it, ++r2it) {
		if (r1it->first != r2it->first || r1it->second != r2it->second)
			return false;
	}
	return true;
}

PolynomRoots& addToFirst(PolynomRoots& roots, const PolynomRoots& whatToAdd) {
	for (const auto& root_degree : whatToAdd) {
		roots[root_degree.first] += root_degree.second;
	}
	return roots;
}

void TestAddToFirst() {
	PolynomRoots roots1, addition{ {1,1} }, result{ {1,1} };
	auto a = roots1;
	addToFirst(roots1, addition);
	ASSERT_EQ(roots1, addition);
}

void addRoot(PolynomRoots& roots, double root) {
	roots[root]++;
}

class Polynom {
public:
	using CC = std::vector<double>;
	Polynom(const std::vector<double> coeffs) : coeffs_(coeffs) {}
	Polynom(std::vector<double>&& coeffs) : coeffs_(std::move(coeffs)) {}

	template<typename T>
	Polynom(T b, T e) : coeffs_(b, e) {}

	Polynom(const std::initializer_list<double>& il) : Polynom(il.begin(), il.end()) {}

	Polynom(const Polynom& other) : coeffs_(other.coeffs_) {}
	Polynom(Polynom&& other) : coeffs_(std::move(other.coeffs_)) {}

	Polynom derivative() const {
		if (degree() == 0) {
			return{0};
		}
		std::vector<double> c;
		int d = coeffs_.size();
		c.reserve(d - 1);
		for (int i = 0; i < coeffs_.size() - 1; ++i) {
			c.push_back((d - i - 1) * coeffs_[i]);
		}
		return Polynom(c);

		//        }
		//        std::vector<double> c;
		//        c.reserve(degree() - 1);
	}

	Polynom rootsToPolynom(const PolynomRoots& rootsWithDegree) {
		for (const auto& rootWithDegree : rootsWithDegree) {
			Polynom p({ 1, -rootWithDegree.first });
		}
	}

	Polynom operator*(const Polynom& other) const {
		CC c(this->degree() + other.degree() + 1);
		for (int i = 0; i < coeffs_.size(); ++i) {
			for (int j = 0; j < coeffs_.size(); ++j) {
				c[i + j] += coeffs_[i] * other.coeffs_[j];
			}
		}
		return Polynom(c);
	}

	bool operator==(const Polynom& other) const {
		return coeffs_ == other.coeffs_;
	}

	bool operator!=(const Polynom& other) const { return !(*this == other); }
	//void normalize() {
	//	// shift coeffs_ so that coeffs_[0] != 0
	//}


	PolynomRoots FindAllRootsAtSegment(double left, double right, double precision) const {
		//// calc def
		//// найти все её корни на данном интервале с учётом кратности
		//// выбираем из них те, которые обнуляют нас, выводим их в ответ (с той кратностью, с какой они корни производной)
		//// на интервалах, на которые разбивают [left, right] найденные точки, бин поиском ищем корни текущего. выводим их в ответ
		std::cout << __FUNCTION__ << "(" << *this << ", " << left << ", " << right << ", " << precision << ")" << std::endl;
		PolynomRoots answer;
		if (degree() == 0) {
			if (coeffs_[0] == 0) {
				throw std::runtime_error(" too many possible roots");
			}
			//else if (isIn(left, right, 0)) {
			//	answer = { { 0, 1 } }; // 0 кратности 1
			//}
			else {
				answer = {}; // no roots       
			}
		}
		else {


			auto deriv = derivative();
			PolynomRoots dRoots;
			try {
				dRoots = deriv.FindAllRootsAtSegment(left, right, precision);
			}
			catch (std::exception e) {
				if (coeffs_[coeffs_.size() - 1] == 0) {
					throw e;
				}
			}

			if (dRoots.empty()) {
				if ((*this)(left) * (*this)(right) < 0) {
					double x = GetRoot(*this, left, right, precision);
					//std::cerr << 
					return{ { x, 1 } };
				}
				else {
					return{};
				}
			}
			else {
				for (auto& root : dRoots) {
					if (isRoot(root.first, precision)) {
						std::cerr << root.first << " is a root of " << *this <<  std::endl;
						answer[root.first] = root.second + 1;
					}
				}

				std::set<double> interValEndsSet;
				for (const auto& root : dRoots) {
					interValEndsSet.insert(root.first);
				}
				interValEndsSet.insert(left);
				interValEndsSet.insert(right);
				std::vector<double> intervalEnds(interValEndsSet.begin(), interValEndsSet.end());

				for (int i = 0; i + 1 < intervalEnds.size(); ++i) {
					auto l = intervalEnds[i], r = intervalEnds[i + 1];
					if ((*this)(l) * (*this)(r) <= 0) {
						double root = GetRoot(*this, l, r, precision);
						addRoot(answer, root);
					}
				}
			}
			//return {GetRoot(*this, left, right);
		//	//}

		//	PolynomRoots roots;
		//	//for (const auto& root : dRoots) {
		//	//	if (isRoot(root.first)) {
		//	//		roots.insert(root);
		//	//	}
		//	//}
		//	//for (const auto& segment : merge(key_iterator(dRoots), Seq<Segment>({ { left, right } }))) {
		//	//	roots.add(GetRoot(*this, segment.left, segment.right));
		//	//}

		//	return roots;
		//}
		}
		std::cout << "\t" << __FUNCTION__ << "(" << *this << ", " << left << ", " << right << ", " << precision << ")" << std::endl;
		std::cerr << "\troots: " << answer << std::endl;
		return answer;
	//	return res;
	}
	
	bool isRoot(const double x, double precision) const {
		auto value = ((*this)(x));
		std::cerr << __FUNCTION__ << "(" << x << ", " << precision << ") <value ==  " << value << "> -- ";
		if (value > -precision && value < precision)
		{
			std::cerr << "true" << std::endl;
			return true;
		}
		std::cerr << "false" << std::endl;
		return false;
	}

	double operator()(const double x) const {
		double res = coeffs_[0];
		for (int i = 1; i < coeffs_.size(); ++i) {
			res *= x;
			res += coeffs_[i];
		}
		return res;
	}

	int degree() const {
		ASSERT_TRUE(coeffs_.size() >= 1);
		return coeffs_.size() - 1;
	}
private:
	std::vector<double> coeffs_;
	friend std::ostream& operator<<(std::ostream& out, const Polynom& p) {
		return out << p.coeffs_;
	}
};

#define TEST_FUNCTION(f)      std::cout << "=====TEST " << #f << std::endl; \
    f(); \
    std::cout << " == OK == " << std::endl;

void PolynomDerivativeTest() {
	{
		Polynom p({ 1 });
		Polynom derivative({ 0 });
		ASSERT_EQ(p.derivative(), derivative);
	}

	Polynom p({ 1,1,1,1 });
	Polynom derivative({ 3,2,1 });
	auto d = p.derivative();
	_c p << "' == " << d _e;
	ASSERT_EQ(d, derivative);
		
}

void PolynomNultiplicationTest() {
	Polynom p1({ 1, 1 });
	Polynom p2({ 1, 1 });
	_c p1 _e
		_c p2 _e

		auto prod = p1 * p2;

	_c prod _e

}

void PolynomRootsTest() {
	PolynomRoots roots{ {1.1, 3} };
	PolynomRoots roots2{ {1.1, 4}, {0, 1} };
	PolynomRoots rootsMergeResult{ {0,1}, {1.1, 7} };

	ASSERT_EQ(roots, roots);
	addToFirst(roots, roots2);
	ASSERT_EQ(roots, rootsMergeResult);
}


struct PolynomRootsTestSet {
	Polynom polynom;
	double leftEnd, rightEnd;
	PolynomRoots roots;
	double precision;
};

std::ostream& operator<<(std::ostream& out, const PolynomRootsTestSet& set) {
	out << "|| polynom = " << set.polynom << std::endl;
	out << "|| on [" << set.leftEnd << ", " << set.rightEnd << "]" << std::endl;
	out << "|| expected roots = " << set.roots << std::endl;
	return out << "|| precision = " << set.precision;
}

bool equalWithPrecision(const PolynomRoots& r1, const PolynomRoots& r2, double precision) {
	if (r1.size() != r2.size()) {
		return false;
	}
	for (auto r1it = r1.begin(), r2it = r2.begin(); r1it != r1.end(); ++r1it, ++r2it) {
		if ( abs(r1it->first - r2it->first) > precision || r1it->second != r2it->second)
			return false;
	}
	return true;
}

void TestPolynomRootsTestSet(const PolynomRootsTestSet set) {
	auto roots = set.polynom.FindAllRootsAtSegment(set.leftEnd, set.rightEnd, set.precision);
	if (!equalWithPrecision(roots, set.roots, set.precision)) {
		ASSERT_EQ(roots, set.roots);
	}
}

void FindAllRootsAtSegmentTest() {
	//////TestPolynomRootsTestSet({ { 0 }, -2, 2,{ { 0, 1 } }, 1e-5 });
	//TestPolynomRootsTestSet({ { 1,1 }, -2, 2,{ { -1, 1 } }, 1e-5});
	//TestPolynomRootsTestSet({ { 1,1 }, -2, -1.5, { }, 1e-5 });
	//TestPolynomRootsTestSet({ { 1,2,1 }, -2, 2, {{-1,2}}, 1e-5 });
	//TestPolynomRootsTestSet({ { 1, -3, 2 }, -4, 4,{ { 1,1 }, {2, 1 }}, 1e-5 });
	TestPolynomRootsTestSet({ { 1, -4, 5, -2 }, -4, 4, { { 1,2 }, { 2, 1 } }, 1e-5 });
}

struct PolynomAtPointValueTestSet{
	Polynom p;
	double x;
	double value;
	double precision;
};

void PolynomAtPointValueTestSetTest(const PolynomAtPointValueTestSet& set) {
	ASSERT_EQ_PRECISION(set.value, set.p(set.x), set.precision);
}

void PolynomAtPointValueTest() {
	PolynomAtPointValueTestSetTest({ {0}, 10, 0, 1e-10 });
	//PolynomAtPointValueTestSetTest({ {3, -8, 5 }, 4. / 3, -1./3,  1e-6 });
}

void RunAllTests() {
	TEST_FUNCTION(PolynomAtPointValueTest);
	TEST_FUNCTION(PolynomNultiplicationTest);
	TEST_FUNCTION(PolynomDerivativeTest);
	TEST_FUNCTION(PolynomRootsTest);
	TEST_FUNCTION(GetRootTest);
	TEST_FUNCTION(FindAllRootsAtSegmentTest);
}

void InitializerListTest() {
	//    _c __FUNCTION__ << "\n";
	//    A({1,2});
}

int main(int argc, const char* argv[]) {
	RunAllTests();
	{
		char c;
		std::cin >> c;
	}
	if (argc >= 2) {
		std::cout << "argv detected" << "\n";
		if (std::strcmp(argv[1], "-test") == 0) {
			GetRootTest();
		}
		else if (std::strcmp(argv[1], "-polynom-test") == 0) {
			_c "RunAllTests();" _e;
			RunAllTests();
		}
		else if (std::strcmp(argv[1], "-list") == 0) {
			_c "-list" << "\n";
			InitializerListTest();
		}
		else {
			std::cout << "No option choosen" << std::endl;
		}
	}
	else {
		std::cout << "<" << argv[0] << ">" << std::endl;
	}
}
