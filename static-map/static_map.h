#pragma once

#include <vector>
#include <string>
#include <utility>
#include <algorithm>

class StaticMap {
public:
    using K = std::string;
    using V = std::string;
    using ItemType = std::pair<K, V>;
    struct ItemsComparator {
        bool operator()(const ItemType& a, const ItemType& b) const {
            return a.first < b.first;
        }
    };

//    bool operator K(const ItemType) {
//    }
    explicit StaticMap(const std::vector<ItemType>& items)
        : items_(items) {
        std::sort(items_.begin(), items_.end(), ItemsComparator());
    }

    bool Find(const std::string& key, std::string* value) const {
        auto possiblePosition = std::lower_bound(items_.begin(), items_.end(), key, [](const ItemType& item, const K& k) {
            return item.first < k;
        });
        if (possiblePosition == items_.end() || possiblePosition->first != key) {
            return false;
        }
        *value = possiblePosition->second;
        return true;
    }
private:
    std::vector<std::pair<std::string, std::string>> items_;
};
