#pragma once

#include <string>
#include <cstring>

class StringView {
public:
    StringView(const std::string& str, size_t pos = 0, size_t len = std::string::npos)
            : s_(str.c_str() + pos), len_(std::min(str.length() - pos, len)) {}
    explicit StringView(const char* cstr)
            : s_(cstr), len_(strlen(cstr)) {}
    StringView(const char* cstr, size_t len)
            : s_(cstr), len_(len) {}
    size_t size() const {
        return len_;
    }
    char operator[](size_t i) const {
        return s_[i];
    }
private:
    const char* s_;
    size_t len_;
};
