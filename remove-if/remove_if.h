#pragma once

#include <stdexcept>

template<class Iterator, class Predicate>
Iterator RemoveIf(Iterator first, Iterator last, Predicate pred) {
    auto write = first;
    for(auto read = first; read != last; ++read) {
        if (!pred(*read)) {
            *write++ = *read;
        }
    }
    return write;
}
