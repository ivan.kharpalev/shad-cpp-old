### `void Lecture(const Student&);`

## Лекция 2: Указатели, Ссылки, Структуры

---

# Что напечатает программа?

```c++

void RemoveSpaces(std::string text) {
    for (size_t i = 0; i < text.size(); ++i) {
        if (text[i] == ' ') {
           text.erase(i, 1)
        }
    }
}

std::string text = "aba ca ba";
RemoveSpaces(text);

std::cout << text << std::endl;
```

 1. `"abacaba\n"`
 2. `"aba ca ba\n"`

---

# Передача параметров

```c++

void Clear(std::string text) { // text - это копия
    text.clear(); // можем делать со строкой что угодно
}

std::string s = "abacaba";

Clear(s); // в функцию передаётся копия переменной

// s не изменилась

```

---

## Как написать функцию `Reverse()`?

```c++
void Reverse(std::string s);

```

Где результат? <!-- Лебовски -->


```c++
std::string Reverse(std::string s);
```

Хотим поменять _эту_ строку, а не создавать новую.

```c++
void Reverse(std::string* pointer_to_s);

```

---

# Указатели

 * Указатель хранит адрес другой переменной в памяти (ссылается на другую переменную)

 * Указатель на переменную типа `T` имеет тип `T*`

`std::string -> std::string*`

* Чтобы получить указатель используют оператор `&`

```c++
int a = 1;
int* b = &a;
```

 * Чтобы обратиться к исходной переменной, указатель нужно разыменовать c помощью оператора `*`.

```c++
*b = 5; // присвоит переменной a значение 5
```

---

# Функция `Reverse()`

```c++
void Reverse(std::string s) {
     for (size_t i = 0; i < s.size() / 2; ++i) {
         char tmp = s[i];
         s[i] = s[s.size() - i - 1];
         s[s.size() - i - 1] = tmp;
     }
}
```

```c++
void Reverse(std::string* s) {
     for (size_t i = 0; i < (*s).size() / 2; ++i) {
         char tmp = (*s)[i];
         (*s)[i] = (*s)[(*s).size() - i - 1];
         (*s)[(*s).size() - i - 1] = tmp;
     }
}

std::string s = "abacaba";

Reverse(&s);
```

---

### Передача параметров по указателю

* Если мы хотим, чтобы функция меняла объект который в неё передали - передаём его по указателю.

Примеры:

```c++
void ReadInput(std::vector<int> *input); // out
```

```c++
void Sort(std::vector<int>* numbers); // in-out
```

```c++
void FindUniqueElements(
    std::vector<int> all_elements,
    std::vector<int>* unique); // out

std::vector<int> all;
ReadInput(&all);

std::vector<int> unique;
FindUniqueElements(all, &unique);
```

---

# Как написать `IsSorted()`

```c++
bool IsSorted(std::vector<int> v);
```

Что если в вектор занимает гигабайт?

```c++
bool IsSorted(std::vector<int>* v);
```

Но функция не меняет вектор внутри.

```c++
bool IsSorted(const std::vector<int>& v);

std::vector<int> v;
ReadInput(&v);
if (IsSorted(v)) {
    std::cout << "input is sorted" << std::endl;
}
```

---

### Передача параметров по константной ссылке

 * Если функция принимает __большой__ объект и не меняет его внутри - передаём этот объект по константной ссылке.

* Компилятор не даст вам изменить объект внутри функции.

Примеры:

```c++
void WriteOutput(const std::vector<int>& answer) {
    for (size_t i = 0; i < answer.size(); ++i) {
        std::cout << answer[i] << std::endl;
    }

    answer[0] = 0; // ошибка компиляции
}
```

```c++
void FindUniqueElements(
    const std::vector<int>& all_elements,
    std::vector<int>* unique);
```

---

# Неконстантная ссылка?

* Нельзя использовать в аргументах функции, потому что в точке вызова нельзя понять меняет функция аргумент или нет.

```c++
std::string pretious;
auto x = Frobricate(s); // что функция делает с s?
```

* Используется в stl (`std::swap`)

```c++
int a = 1, b = 2;
std::swap(a, b);
// b == 1, a == 2

void Reverse(std::string* s) {
    for (size_t i = 0; i < (*s).size() / 2; ++i) {
        std::swap((*s)[i], (*s)[(*s).size() - i - 1]);
    }
}
```

---

# Return value optimization

```c++
std::vector<std::vector<int>> MakeZeroMatrix(size_t n) {
    std::vector<std::vector<int>> matrix;
    matrix.resize(n);
    for (size_t i = 0; i < matrix.size(); ++i) {
        matrix[i].resize(n);
    }
    return matrix;
}

std::vector<std::vector<int>> zero = MakeZeroMatrix(128);
```

* Компилятор оптимизирует копирование возвращаемого значения.

---

# Итог

 * Параметр простого типа (`int`, `char`).
   * Передаём по значению. `void f(int x);`
 * Тяжёлый объет (`vector`, `string`).
   * Передаем по константной ссылке
     `void f(const std::vector<int>& x);`
   * Передаём по указателю, если надо изменить
     `void f(std::string* s);`
 * Возвращать тяжёлый объект нужно по значению.
   * Компилятор применит RVO
     `std::vector<int> ReadInput();`

---

# Структуры

```c++
struct LogRecord {
    std::string url, cookie;
    size_t response_size;
};
```

```c++
void WriteLogRecord(const LogRecord& r);
```

```c++
LogRecord record;
record.url = "http://yandex.ru";
record.cookie = "yuid=adsfadfqwer134t13u1t98u45t2t5";

WriteLogRecord(record);
```

---

# Структуры

```c++
struct LogRecord { 
    std::string url, cookie;
    size_t response_size;

    // на самом деле
    LogRecord() = default;

    // Обсудим на следующих лекциях
    LogRecord(const LogRecord&) = default;
    LogRecord(LogRecord&&) = default;
    LogRecord& operator = (const LogRecord&) = default;
    LogRecord& operator = (LogRecord&&) = default;
    ~LogRecord() = default;
};
```

---

# Структуры

```c++
struct LogRecord { 
    std::string url, cookie;
    size_t response_size;

    LogRecord(const std::string& request_url,
              size_t responze_body_size)
        : url(request_url),
          response_size(response_body_size) {}
};
```

```c++
LogRecord invalid; // ошибка компиляции
LogRecord correct("http://yandex.ru", 12341);
```

---

# Массивы и арифметика указателей

```c++
char buffer[1024];

buffer[1023] = 0;

char* ptr = buffer;

*ptr == buffer[0];

ptr = ptr + 5;

*ptr == buffer[5];
```

* Если вам в коде надо использовать обычные массивы - скорее всего вы пишите код неправильно(*).