#pragma once
#include <iostream>

class Matrix {
public:
    using Row = std::vector<int>;
    Matrix(const Matrix& m)
            : rows_{m.rows_} {}
    explicit Matrix(size_t n) : Matrix(n, n) {}
    explicit Matrix(size_t n, size_t m)
            : rows_(n, Row(m)) { }
    Matrix(const std::vector<Row>& rows)
            : rows_(rows) { }
    Matrix& operator=(const Matrix& m) {
        rows_ = m.rows_;
        return *this;
    }
    size_t Rows() const {
        return rows_.size();
    }
    size_t Columns() const {
        return rows_.empty() ? 0 : rows_[0].size();
    }
    int& operator()(size_t i, size_t j) {
        return rows_.at(i).at(j);
    }
    const int& operator()(size_t i, size_t j) const {
        return rows_.at(i).at(j);
    }
    Matrix& operator+=(const Matrix& m) {
        for (size_t i = 0; i < m.Rows(); ++i) {
            for (size_t j = 0; j < m.Columns(); ++j) {
//                rows_[i][j] += m.rows_[i][j];
                (*this)(i,j) += m(i,j);
            }
        }
        return *this;
    }
    Matrix operator+(const Matrix& m) const {
        auto res = *this;
        res += m;
        return res;
    }
    Matrix operator-() const {
        auto m = *this;
        for (size_t i = 0; i < Rows(); ++i) {
            for (size_t j = 0; j < Columns(); ++j) {
                auto &e = m.rows_[i][j];
                e = -e;
            }
        }
        return m;
    }
    Matrix operator-=(const Matrix& m) {
        *this += -m;
        return *this;
    }
    Matrix operator*(const Matrix& m) const {
        Matrix res(Rows(), m.Columns());
//        std::cout << Rows() << "x" << Columns() << " * " << m.Rows() << "x" << m.Columns() << std::endl;
        for (size_t i = 0; i < Rows(); ++i) {
            for (size_t j = 0; j < m.Columns(); ++j) {
                for (size_t k = 0; k < Columns(); ++k) {
//                    std::cout << i << " " << j << " " << k << std::endl;
                    res(i,j) += (*this)(i, k) * m(k, j);
                }
            }
        }
//        std::cout << "operator*() const exit" << std::endl;
        return res;
    }

    Matrix& operator*=(const Matrix& m) {
        auto prod = *this * m;
        *this = prod;
        return *this;
    }

    Matrix operator-(const Matrix& m) const {
        return *this + (-m);
    }
private:
    std::vector<Row> rows_;
};

Matrix Identity(size_t n) {
    return Matrix(n);
}

Matrix Transpose(const Matrix& m) {
    Matrix t(m.Columns(), m.Rows());
    for (size_t i = 0; i < m.Rows(); ++i) {
                for (size_t j = 0; j < m.Columns(); ++j) {
            t(j, i) = m(i, j);
        }
    }
    return t;
}