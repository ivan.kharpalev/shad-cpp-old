#include <testing.h>
#include <matrix.h>

bool EqualMatrix(const Matrix& a, const Matrix& b) {
    if (a.Rows() != b.Rows())
        return false;
    if (a.Columns() != b.Columns())
        return false;
    for (size_t i = 0; i < a.Rows(); ++i)
        for (size_t j = 0; j < a.Columns(); ++j)
            if (a(i, j) != b(i, j))
                return false;
    return true;
}

namespace tests {

void Constructors() {
    {
        Matrix a(3);
        ASSERT_EQ(3u, a.Rows());
        ASSERT_EQ(3u, a.Columns());
    }
    {
        Matrix a(3, 5);
        ASSERT_EQ(3u, a.Rows());
        ASSERT_EQ(5u, a.Columns());
    }
    {
        std::vector<std::vector<int>> v({
                                                {1, 2},
                                                {3, 4},
                                                {5, 6}
                                        });
        Matrix a(v);
        ASSERT_EQ(3u, a.Rows());
        ASSERT_EQ(2u, a.Columns());
        ASSERT_EQ(3, a(1, 0));
    }
}

void Constness() {
    {
        Matrix a({{1, 2}, {3, 4}});
        const Matrix& b = a;
        ASSERT_EQ(2u, b.Rows());
        ASSERT_EQ(2u, b.Columns());
        ASSERT_EQ(2, b(0, 1));
    }
    {
        const Matrix a = Identity(3);
        const Matrix b(3);
        ASSERT_EQ(true, EqualMatrix(a, Transpose(a)));
        ASSERT_EQ(true, EqualMatrix(b, a - a));
        ASSERT_EQ(true, EqualMatrix(a, a * a));
    }
}

void Operations() {
    Matrix a({
        {1, 2, 3},
        {4, 5, 6},
    });
    Matrix b({
        {0, 1, 0},
        {1, 1, 2}
    });
    Matrix c({
        {-1, -1},
        {1, 1},
        {1, -1}
    });

    ASSERT_EQ(false, EqualMatrix(a, Transpose(a)));
    ASSERT_EQ(true, EqualMatrix(Transpose(a), Matrix({
        {1, 4},
        {2, 5},
        {3, 6}})));

    Matrix old_a = a;
    ASSERT_EQ(true, EqualMatrix(a += b, Matrix({
        {1, 3, 3},
        {5, 6, 8}})));
    ASSERT_EQ(true, EqualMatrix(a -= b, old_a));
    ASSERT_EQ(true, EqualMatrix(a -= a, Matrix(2, 3)));

    ASSERT_EQ(true, EqualMatrix(b * c, Matrix({
        {1, 1},
        {2, -2}
    })));
}
#ifdef OLOLO
#endif
void TestAll() {
    StartTesting();
    RUN_TEST(Constructors);
    RUN_TEST(Constness);
    RUN_TEST(Operations);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
