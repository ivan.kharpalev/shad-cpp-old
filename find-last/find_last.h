#pragma once

#include <stdexcept>

template<class Iterator, class T>
Iterator FindLast(Iterator first, Iterator last, const T& val) {
    if (first == last) {
        return last;
    }
    auto it = last;
    do {
        --it;
        if (*it == val) {
            return it;
        }
    } while (first != it);
    return last;
}
