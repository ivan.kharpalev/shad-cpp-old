#include "lru_cache.h"
#include <iostream>
#include <string>

int main() {
	LruCache cach(1);

	std::string k1 = "a", k2 = "b", v1 = "A";
	cach.Set("a", "A");

	std::string s;
	{
		int res = cach.Get("a", &s);
		std::cout << "look for <" << k1 << "> " << " found == " << res << std::endl;
	}
	cach.Set("b", "B");
	{
		int res = cach.Get("a", &s);
		std::cout << "look for <" << k1 << "> " << " found == " << res << std::endl;
	}

}