#pragma once

#include <list>
#include <string>
#include <unordered_map>


class LruCache {
public:
	using KeyType = std::string;
	using ValueType = std::string;
	LruCache(size_t max_size) : max_size_(max_size) {}

	void Set(const KeyType& key, const ValueType& value) {
		if (map_.size() == max_size_) {
			map_.erase(list_.front());
			list_.erase(list_.begin());
		}
		auto location = map_.find(key);
		if (location != map_.end()) {
			list_.erase(location->second.second);
			auto listBegin = list_.begin();
			location->second = std::make_pair(value, listBegin);
		}
		else {
			list_.emplace_front(key);
			auto listBegin = list_.begin();
			//int i = listBegin;
			//std::cout << listBegin << std::endl;
			map_[key].first = value;
			map_[key].second = listBegin;// (key, std::make_pair(value, listBegin));
		}
	}

	bool Get(const KeyType& key, ValueType* value) { 
		auto location = map_.find(key);
		if (location == map_.end()) {
			return false;
		}
		list_.erase(location->second.second);
		list_.emplace_front(key);
		*value = location->second.first;
		return true;
	}

private:
	using ListType = std::list<KeyType>; // TODO user MapType::iterator here instead of KeyType
	using MapType = std::unordered_map<KeyType, std::pair<ValueType, ListType::iterator>>;
	MapType map_;
	ListType list_;
	size_t max_size_;
};
