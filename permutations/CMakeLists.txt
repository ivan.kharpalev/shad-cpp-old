cmake_minimum_required(VERSION 2.8)
project(permutations)

if (TEST_SOLUTION)
  include_directories(../private/permutations)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_permutations test.cpp)
