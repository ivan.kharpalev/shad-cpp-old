#include <testing.h>
#include <range.h>
#include <vector>

namespace tests {

std::vector<int> empty;

void Simple() {
    {
        std::vector<int> expected{2, 3, 4};
        ASSERT_EQ(expected, Range(2, 5, 1));
    }
    {
        std::vector<int> expected{1, 3};
        ASSERT_EQ(expected, Range(1, 5, 2));
    }
    {
        std::vector<int> expected{-9, -4, 1, 6};
        ASSERT_EQ(expected, Range(-9, 10, 5));
    }
}

void SimpleReverse() {
    {
        std::vector<int> expected{5, 4, 3};
        ASSERT_EQ(expected, Range(5, 2, -1));
    }
    {
        std::vector<int> expected{5, 3};
        ASSERT_EQ(expected, Range(5, 1, -2));
    }
    {
        std::vector<int> expected{7};
        ASSERT_EQ(expected, Range(7, 6, -3));
    }
}

void Empty() {
    ASSERT_EQ(empty, Range(0, 0, 2));
    ASSERT_EQ(empty, Range(2, 2, 1));
    ASSERT_EQ(empty, Range(10, 10, -2));
}

void EmptyInf() {
    ASSERT_EQ(empty, Range(3, 7, -1));
    ASSERT_EQ(empty, Range(3, 4, 0));
    ASSERT_EQ(empty, Range(5, -5, 2));
    ASSERT_EQ(empty, Range(3, -7, 0));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(SimpleReverse);
    RUN_TEST(Empty);
    RUN_TEST(EmptyInf);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
