#include <testing.h>
#include <util.h>
#include <ring_buffer.h>
#include <iostream>

namespace tests {

void TestEmpty() {
    RingBuffer buffer(1);

    ASSERT_EQ(0u, buffer.Size());
    ASSERT_EQ(true, buffer.Empty());
}

void TestPushAndPop() {
    RingBuffer buffer(2);

    int i;
    ASSERT_EQ(true, buffer.TryPush(0));
    ASSERT_EQ(true, buffer.TryPush(1));
    std::cout << buffer << "  " << __LINE__ << std::endl;
    ASSERT_EQ(false, buffer.TryPush(2));

    std::cout << buffer << "  " << __LINE__ << std::endl;
    ASSERT_EQ(2u, buffer.Size());
    std::cout << buffer << "  " << __LINE__ << std::endl;
    ASSERT_EQ(false, buffer.Empty());
    std::cout << buffer << "  " << __LINE__ << std::endl;

    ASSERT_EQ(true, buffer.TryPop(&i));
    std::cout << buffer << "  " << __LINE__ << std::endl;
    ASSERT_EQ(0, i);
    ASSERT_EQ(true, buffer.TryPop(&i));
    ASSERT_EQ(1, i);

    ASSERT_EQ(false, buffer.TryPop(&i));
    ASSERT_EQ(0u, buffer.Size());
    ASSERT_EQ(true, buffer.Empty());
}

void TestRandom() {
    RandomGenerator rnd(73467534);
    RingBuffer buffer(1);

    int next_element = 0;
    int next_received_element = 0;
    for (int i = 0; i < 100000; ++i) {
        if (rnd.GenInt(0, 1) == 0) {
            if (bool res = buffer.TryPush(next_element)) {
                std::cout << buffer << "  " << __LINE__ << " push " << next_element << " res=" << res << " size=" << buffer.Size() << std::endl;
                next_element++;
            }
        } else {
            int element;
            if (bool res = buffer.TryPop(&element)) {
                std::cout << buffer << "  " << __LINE__ << " pop " << element << " res=" << res << " size=" << buffer.Size() << std::endl;

                ASSERT_EQ(next_received_element, element);
                next_received_element++;
            }
        }
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(TestEmpty);
    RUN_TEST(TestPushAndPop);
    RUN_TEST(TestRandom);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
