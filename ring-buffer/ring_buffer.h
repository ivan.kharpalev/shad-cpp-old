#pragma once
#include <ostream>

class RingBuffer {
public:
    explicit RingBuffer(size_t capacity)
    {
        v_.resize(capacity);
    }

    size_t Size() const {
        return len_;
    }

    bool Empty() const {
        return Size() == 0;
    }

    bool TryPush(int element) {
        if (!HasPlace()) {
            return false;
        }
        v_[(begin_ + len_) % v_.size()] = element;
        ++len_;
        return true;
    }

    bool TryPop(int *element) {
        if (Empty()) {
            return false;
        }
        *element = v_[begin_];
        IncIndex(&begin_);
        --len_;
        return true;
    }

    friend std::ostream& operator<<(std::ostream& out, const RingBuffer& rb) {
        out << "(";
        for (int i : rb.v_) {
            out << i << ", ";
        }
        return out << ") b=" << rb.begin_ << ", len=" << rb.len_;
    }

private:
    void IncIndex(size_t *i) {
        (*i)++;
        *i %= v_.size();
    }

    bool HasPlace() const {
        return Size() != v_.size();
    }
    std::vector<int> v_;
    size_t begin_ = 0;
    size_t len_ = 0;
};
