#pragma once

#include <cstdint>

class Stack {
public:
    void Push(int x) {
        stack_.push_back(x);
    }

    bool Pop() {
        if (stack_.empty()) {
            return false;
        }
        stack_.resize(stack_.size() - 1);
        return true;
    }

    int Top() const {
        return stack_.back();
    }

    bool Empty() const {
        return stack_.empty();
    }

    size_t Size() const {
        return stack_.size();
    }
private:
    std::vector<int> stack_;
};
