#include <testing.h>
#include <util.h>
#include <fold.h>

#include <algorithm>
#include <list>

namespace tests {

void JustSum() {
    std::list<int> a{1, 2, 3, 6};
    ASSERT_EQ(12, Fold(a.cbegin(), a.cend(), 0, Sum()));
}

void JustProd() {
    std::list<int> a{-1, 1, 2, 3};
    ASSERT_EQ(-6, Fold(a.cbegin(), a.cend(), 1, Prod()));
}

void Vectors() {
    std::vector<std::vector<int>> v{
        {1, 2},
        {3},
        {4, 5, 6},
        {}
    };
    std::vector<int> expected{1, 2, 3, 4, 5, 6};
    ASSERT_EQ(expected, Fold(v.begin(), v.end(), std::vector<int>(), Concat()));
}

void SequenceLength() {
    {
        std::list<int> a{1, 3, -5, 4};
        int cnt = 0;
        Length f(&cnt);
        Fold(a.begin(), a.end(), 0, f);
        ASSERT_EQ(4, cnt);
    }
    {
        std::list<std::string> a{"aba", "caba"};
        int cnt = 0;
        Length f(&cnt);
        Fold(a.begin(), a.end(), std::string(), f);
        ASSERT_EQ(2, cnt);
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(JustSum);
    RUN_TEST(JustProd);
    RUN_TEST(Vectors);
//    RUN_TEST(SequenceLength);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
