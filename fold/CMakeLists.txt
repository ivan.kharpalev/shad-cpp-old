cmake_minimum_required(VERSION 2.8)
project(fold)

if (TEST_SOLUTION)
  include_directories(../private/fold)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)
endif()

add_executable(test_fold test.cpp)
