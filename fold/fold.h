#pragma once

struct Sum {
//    template<class T>
//    struct Summer {
//        Summer(const T& t) : value(t) {}
//        Subber& operator()(const T& x) {
//            value += x;
//        }
//        T operator()() const {
//            return value;
//        }
//        T value;
//    };
//
//    template<class T>
//    Summer<T> init(const T& t) {
//        return Summer<T>(t);
//    }
    template<class T>
    T operator()(const T& a, const T& b) const {
        return a + b;
    }
};

struct Prod {
    template<typename T>
    T operator()(const T& a, const T&b) const {
        return a * b;
    }
};

struct Concat {
    template<typename T>
    T operator()(const T& a, const T&b) const {
        T res;
        std::copy(b.begin(), b.end(),
                  std::copy(a.begin(), a.end(), std::back_inserter(res)));
        return res;
    }
};

template<class Iterator, class T, class BinaryOp>
T Fold(Iterator first, Iterator last, T init, BinaryOp func) {
    while (first != last) {
        init = func(init, *first);
        ++first;
    }
    return init;
}

class Length {
public:
    Length(int *place) : len_(*place) {
        len_ = 0;
    }
    template<typename T>
    T operator()(const T& a, const T&) {
        ++len_;
        return a;
    }
private:
    int& len_;
};
