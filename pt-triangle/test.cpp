#include <testing.h>
#include <point_triangle.h>

namespace tests {

void TestAll() {
    StartTesting();
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
