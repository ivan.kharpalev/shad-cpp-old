#pragma once

#include <stdexcept>

template<class InputIterator, class OutputIterator>
OutputIterator Copy(InputIterator first, InputIterator last, OutputIterator result) {
    while (first != last) {
        *result++ = *first++;
    }
    return result;
}

template<class Iterator1, class Iterator2, class OutIterator>
OutIterator Merge(Iterator1 first1, Iterator1 last1,
                  Iterator2 first2, Iterator2 last2,
                  OutIterator result) {
    auto it1 = first1;
    auto it2 = first2;
    while(it1 != last1 && it2 != last2) {
        if (*it1 < *it2) {
            *result++ = *it1++;
        }
        else if (*it2 < *it1) {
            *result++ = *it2++;
        }
        else {
            *result++ = *it1++;
            *result++ = *it2++;
        }
    }
    if (it1 != last1) {
        Copy(it1, last1, result);
    }
    if (it2 != last2) {
        Copy(it2, last2, result);
    }
    return result;
}
