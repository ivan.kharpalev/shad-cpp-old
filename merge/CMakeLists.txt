cmake_minimum_required(VERSION 2.8)
project(merge)

if (TEST_SOLUTION)
  include_directories(../private/merge)
endif()

include(../common.cmake)

set(SRC test.cpp)

if (ENABLE_PRIVATE_TESTS)
    set(SRC ../private/merge/test.cpp)
endif()

add_executable(test_merge ${SRC})
