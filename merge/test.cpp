#include <testing.h>
#include <util.h>
#include <strict_iterator.h>
#include <merge.h>

struct A {
    int n, m;
    bool operator<(const A& other) const {
        return n < other.n;
    }
    bool operator==(const A& other) const {
        return n == other.n && m == other.m;
    }
};

std::ostream& operator<<(std::ostream& out, const A& a) {
    return out << "(" << a.n << ", " << a.m <<")";
}

std::ostream& operator<<(std::ostream& out, const std::vector<A>& v) {
    out << '(';
    if (!v.empty()) {
        out << v[0];
        for (size_t i = 1; i < v.size(); ++i) {
            out << ", " << v[i];
        }
    }
    return out << ')';
}

namespace tests {

    void SimpleTest() {
        std::vector<A> a{{{1,0}, {2, 0}, {3, 0}, {4,0}}}, b{{{1,1}, {3, 1},{3,2}}},
        c{{{1,0}, {1, 1}, {2, 0}, {3, 0}, {3,1}, {3, 2}, {4,0}}}, res;
        Merge(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(res));
        ASSERT_EQ(res, c);
    }
void TestAll() {
    StartTesting();
    SimpleTest();
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
