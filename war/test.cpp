#include <testing.h>
#include <war.h>
#include <array>
#include <algorithm>
#include <vector>

using Deck = std::array<int, 5>;

namespace tests {

void Simple() {
    {
        Deck f{1, 3, 5, 7, 9};
        Deck s{2, 4, 6, 8, 0};
        auto result = SimulateWarGame(f, s);
        ASSERT_EQ(SECOND, result.winner);
        ASSERT_EQ(5, result.turn);
    }
}

void SomeTests() {
    {
        Deck f{1, 2, 3, 4, 0};
        Deck s{5, 6, 7, 8, 9};
        auto result = SimulateWarGame(f, s);
        ASSERT_EQ(SECOND, result.winner);
        ASSERT_EQ(17, result.turn);
    }
    {
        Deck f{0, 8, 4, 5, 7};
        Deck s{9, 1, 2, 6, 3};
        auto result = SimulateWarGame(f, s);
        ASSERT_EQ(FIRST, result.winner);
        ASSERT_EQ(15, result.turn);
    }
}

void Big() {
    Deck f{0, 1, 2, 3, 6};
    Deck s{4, 7, 8, 9, 5};
    auto result = SimulateWarGame(f, s);
    ASSERT_EQ(FIRST, result.winner);
    ASSERT_EQ(327, result.turn);
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(SomeTests);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
