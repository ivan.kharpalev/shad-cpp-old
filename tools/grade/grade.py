#!/usr/bin/python3
"""Task grading.

Usage:
  grade.py pre-release-check [--task=<task-name>] [--dry-run]
  grade.py grade [--dry-run]
  grade.py clean [--dry-run]

Options:
  --task=<task-name>
  --dry-run                 Don't execute any commads, just print when to stdout. [default: False]

"""
import yaml
import glob
import shutil
import time
import json
import os
import os.path
import sys
import pprint
import json
import collections
import subprocess
import re
import logging
import docopt
import requests
import codecs
import pwd
import grp
import resource


logger = logging.getLogger(__name__)


def print_info(*args):
    print(*args, file=sys.stderr)
    sys.stderr.flush()


class PipelineBuilder:
    def __init__(self):
        self.cmds = []
        self.ctx = {}

    def run(self, cmd, **kwargs):
        self.cmds.append((cmd, kwargs))

    def run_in_sandbox(self, cmd, **kwargs):
        ENV_WHITELIST = ["PATH"]
        TIMEOUT_SECONDS = 60

        def set_up_sandbox():                    
            try:
                uid = pwd.getpwnam("nobody").pw_uid
                gid = grp.getgrnam("nogroup").gr_gid
                os.setgroups([])
                os.setresgid(gid, gid, gid)
                os.setresuid(uid, uid, uid)
            except:
                print("WARNING: UID and GID change failed, running with current user",
                      file=sys.stderr)

            env = os.environ.copy()
            os.environ.clear()
            for variable in ENV_WHITELIST:
                os.environ[variable] = env[variable]

        self.run(cmd, preexec_fn=set_up_sandbox, timeout=TIMEOUT_SECONDS, **kwargs)

    def execute(self, dry_run=False):
        for cmd, kwargs in self.cmds:
            if isinstance(cmd, list) or isinstance(cmd, str):
                redirect_hook = None
                report = None # Extend fd lifetime past fork.
                if "redirect_hook" in kwargs:
                    redirect_hook = kwargs["redirect_hook"]
                    del kwargs["redirect_hook"]

                    cmd, report = redirect_hook(cmd, dry_run)

                if isinstance(cmd, str):
                    cmdline = cmd
                else:
                    cmdline = " ".join(cmd)

                if "preexec_fn" in kwargs:
                    cmdline = "sandbox " + cmdline

                if "cwd" in kwargs:
                    cmdline = "cd {} && ".format(kwargs["cwd"]) + cmdline
                print_info(">", cmdline)

                if dry_run:
                    continue

                if 'check' not in kwargs:
                    kwargs['check'] = True
                subprocess.run(cmd, close_fds=False, **kwargs)
            elif callable(cmd):
                args=", ".join("{}={}".format(k, repr(v)) for k, v in sorted(kwargs.items()))
                print_info("$ {}({})".format(cmd.__name__, args))

                if dry_run:
                    continue
                
                cmd(self.ctx, **kwargs)


def check_regexp(ctx, pattern, regexps):
    for filename in glob.glob(pattern):
        file_content = codecs.open(filename, encoding='utf-8').read()
        for regexp in regexps:
            if re.search(regexp, file_content, re.MULTILINE):
                raise ValueError("File {} contains banned regexp '{}'".format(
                    filename, regexp))

    
class Task:
    def __init__(self, name, config):
        self.name = name
        self._config = config

        self.review = config.get("review", False)

        self.sources = config.get("allow_change", [])
        if not self.sources:
            raise ValueError("No sources specified in 'allow_change' for %s" % name)
        if not isinstance(self.sources, list):
            self.sources = [self.sources]

        for src in self.sources:
            if not glob.glob(os.path.join(name, src)):
                raise ValueError("Source file %s not found in %s" % (src, name))

        self.forbidden_regexp = config.get("forbidden_regexp", [])
        # Cheap and dirty way to protect against
        # #include <opt/task_name/solution.cpp>
        self.forbidden_regexp += ["include.*{}[.]cpp.*".format(self.name)]
        for regexp in self.forbidden_regexp:
            re.compile(regexp)

        self.disable_sanitizers = config.get("disable_sanitizers", False)

        self.tests = config.get("tests", [])
        if not isinstance(self.tests, list):
            self.tests = [self.tests]
        
        self.solutions = config.get("solutions", [])
        if not isinstance(self.solutions, list):
            self.solutions = [self.solutions]
        self.private_tests = config.get("private_tests", False)

        self.review = config.get("review", False)

    def __repr__(self):
        return "Task(name='{}')".format(self.name)
        
    def build_name(self, build_type, solution=None):
        build_name = build_type
        if solution is not None:
            build_name += "_" + solution
        return build_name

    def gen_build(self, p, build_type, solution=None, with_private_tests=True):
        build_name = self.build_name(build_type, solution)
        build_dir = os.path.join(self.name, "build", build_name)
            
        p.run(["mkdir", "-p", build_dir])
        p.run(["cmake", "../../", "-G", "Ninja",
               "-DCMAKE_BUILD_TYPE=" + build_type,
               "-DENABLE_PRIVATE_TESTS=1"] +
              ([] if solution is None else ["-DTEST_SOLUTION=" + solution]),
              cwd=build_dir)
        p.run(["ninja", "-v", "-C", build_dir])

    def gen_tests(self, p, build_type, test, solution=None, sandbox=False):
        build_name = self.build_name(build_type, solution)
        cmd = ["./build/{}/{}".format(build_name, test)]
        kwargs = {"cwd": self.name}

        if sandbox:
            p.run_in_sandbox(cmd, **kwargs)
        else:
            p.run(cmd, **kwargs)

    def copy_sources(self, p, submit_root):
        def copy_src(ctx, relpath):
            os.makedirs(os.path.dirname(relpath), exist_ok=True)
            shutil.copyfile(os.path.join(submit_root, relpath), relpath)

        for pattern in self.sources:
            for src in glob.glob(os.path.join(submit_root, self.name, pattern)):
                relpath = os.path.relpath(src, submit_root)
                p.run(copy_src, relpath=relpath)

            if self.forbidden_regexp:
                p.run(check_regexp, pattern=os.path.join(self.name, pattern),
                      regexps=self.forbidden_regexp)

    def pre_release_check(self, p):
        p.run(["mkdir", "-p", os.path.join(self.name, "build")])

        for solution in self.solutions:
            build_type = "release" if self.disable_sanitizers else "asan"
            
            self.gen_build(p, build_type, solution)
            for test in self.tests:
                self.gen_tests(p, build_type, test, solution)

    def grade(self, p, user_id):
        p.run(["mkdir", "-p", os.path.join(self.name, "build")])

        build_type = "release" if self.disable_sanitizers else "asan"
        self.gen_build(p, build_type)
        for test in self.tests:
            self.gen_tests(p, build_type, test, sandbox=True)

        if not self.review:
            p.run(self.push_report, user_id=user_id)

    def push_report(self, ctx, user_id):
        # Do not expose token in logs.
        tester_token = os.environ["TESTER_TOKEN"]
        for _ in range(3):
            rsp = requests.post("https://ctf.best-cpp-course-ever.ru/api/report",
                data={"token": tester_token, "task": self.name, "user_id": user_id})

            if rsp.status_code != 500:
                break
            else:
                time.sleep(1.0)

        rsp.raise_for_status()

    def clean(self, p):
        p.run(["rm", "-rf", os.path.join(self.name, "build")], check=False)


class Course:
    def __init__(self, config):
        self._config = config

        self.tasks = collections.OrderedDict()
        self.groups = []
        
        for group in config:
            group_name = group["name"]

            task_group = []
            for _, task_name in group.get("tasks", []):
                config = json.load(open(os.path.join(task_name, ".tester.json")))
                if config.get("type", "") == "ctf":
                    continue
                
                task = Task(task_name, config)
                task_group.append(task)

                self.tasks[task_name] = task

            self.groups.append({
                "name": group_name,
                "tasks": task_group,
            })

    def clean(self, p):
        for task in self.tasks.values():
            task.clean(p)

    def pre_release_check(self, p, task=None):
        names = [task] if task is not None else self.tasks
        for task in names:
            self.tasks[task].pre_release_check(p)

    def grade(self, p, task_name, submit_root, user_id):
        if task_name not in self.tasks:
            raise ValueError("Task %s not found" % task_name)

        task = self.tasks[task_name]
        if submit_root is not None and submit_root != ".":
            task.copy_sources(p, submit_root)

#        task.lint(p)
        task.grade(p, user_id)



def grade_on_ci(course, pipeline):
    task_name = os.environ["CI_COMMIT_REF_NAME"].split("/")[1]
    submit_root = os.environ["CI_PROJECT_DIR"]
    user_id = os.environ["GITLAB_USER_ID"]
    
    course.grade(pipeline, task_name, submit_root, user_id)

        
if __name__ == "__main__":
    args = docopt.docopt(__doc__, version='Grade Tool 3.0')

    course = Course(yaml.load(open(".deadlines.yml")))

    p = PipelineBuilder()
    
    if args["clean"]:
        course.clean(p)
    elif args["pre-release-check"]:
        course.pre_release_check(p, args["--task"])
    elif args["grade"]:
        grade_on_ci(course, p)

    p.execute(dry_run=args["--dry-run"])
