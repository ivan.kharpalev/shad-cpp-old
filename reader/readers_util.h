#pragma once

#include <vector>
#include <memory>

#include <reader.h>

class LimitReader: public Reader {
public:
    LimitReader(std::unique_ptr<Reader> reader, size_t limit) :
            limit_(limit), reader_(std::move(reader)) {

    }

    virtual size_t Read(char* buf, size_t len) override {
        const size_t CHUNK_SIZE = 128;

        auto lenWanted = len;
        std::string chunk;
        while (len > 0) {
            size_t lenToRead = std::min(limit_, len);
            size_t read_res = reader_->Read(buf, lenToRead);
            if (read_res == 0) {
                break;
            }
            buf += read_res;
            len -= read_res;
            limit_ -= read_res;
        }
        return lenWanted - len;
    }
private:
    size_t limit_;
    std::unique_ptr<Reader> reader_;
};

class TeeReader: public Reader {
public:
    TeeReader(std::vector<std::unique_ptr<Reader>> readers) :
            readers_{std::move(readers)} {

    }

    virtual size_t Read(char* buf, size_t len) override {
        auto lenOriginal = len;
        while (len > 0 && current_reader_ < readers_.size()) {
            size_t read_res = readers_[current_reader_]->Read(buf, len);
            if (read_res == 0) {
                readers_[current_reader_].release();
                ++current_reader_;
                continue;
            }
            buf += read_res;
            len -= read_res;
        }
        return lenOriginal - len;
    }
private:
    std::vector<std::unique_ptr<Reader>> readers_;
    size_t current_reader_ = 0;
};

class HexDecodingReader: public Reader {
public:
    HexDecodingReader(std::unique_ptr<Reader> reader) {}

    virtual size_t Read(char* buf, size_t len) override {
        return 0;
    }
};
