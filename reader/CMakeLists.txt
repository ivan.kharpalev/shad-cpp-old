cmake_minimum_required(VERSION 2.8)
project(reader)

if (TEST_SOLUTION)
  include_directories(../private/reader)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_reader test.cpp)
