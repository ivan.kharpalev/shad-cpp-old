#pragma once

#include <stdexcept>

template<class Iterator, class Predicate>
Iterator Partition(Iterator first, Iterator last, Predicate pred) {
    if (first == last) {
        return last;
    }
//    if (std::next(first) == last) {
//        if (pred(*first)) {
//            return last;
//        }
//        return first;
//    }
    --last;
    while(first != last) {
        while(first != last && pred(*first)) {
            ++first;
        }
        while(first != last && !pred(*last)) {
            --last;
        }
        std::swap(*first, *last);
    }
    if (pred(*last)) {
        return std::next(last);
    }
    return last;
}
