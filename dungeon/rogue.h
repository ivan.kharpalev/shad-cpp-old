#pragma once

#include "dungeon.h"

#include <stdexcept>
#include <queue>

Room* FindFinalRoom(Room* starting_room) {
    std::vector<Door*> closedDoors;
    std::vector<std::string> keys;
    std::queue<Room*> unprocessedRoom{{starting_room}};
    while(!unprocessedRoom.empty()) {
        auto& room = *unprocessedRoom.front();
        if (room.IsFinal()) {
            return &room;
        }
        unprocessedRoom.pop();
        for(size_t i = 0; i < room.NumKeys(); ++i) {
            auto key = room.GetKey(i);
            for (size_t j = 0; j < closedDoors.size(); ++j) {
                auto& door = *closedDoors[j];
                if (door.TryOpen(key)) {
                    unprocessedRoom.push(door.GoThrough());
                    closedDoors.erase(closedDoors.begin() + i);
                }
            }
            keys.push_back(key);
        }
        for (size_t i = 0; i < room.NumDoors(); ++i) {
            auto& door = *room.GetDoor(i);
            for (const auto& key : keys) {
                if (door.TryOpen(key)) {
                    unprocessedRoom.push(door.GoThrough());
                }
            }
        }
    }
    return nullptr;
//    throw std::runtime_error("Not implemented");
}
